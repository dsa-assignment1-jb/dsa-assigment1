import ballerina/grpc;

public isolated client class userMangerClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function greatuser(User|ContextUser req) returns (response|grpc:Error) {
        map<string|string[]> headers = {};
        User message;
        if (req is ContextUser) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("userManger/greatuser", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <response>result;
    }

    isolated remote function greatuserContext(User|ContextUser req) returns (ContextResponse|grpc:Error) {
        map<string|string[]> headers = {};
        User message;
        if (req is ContextUser) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("userManger/greatuser", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <response>result, headers: respHeaders};
    }

    isolated remote function addUser(User|ContextUser req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        User message;
        if (req is ContextUser) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("userManger/addUser", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function addUserContext(User|ContextUser req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        User message;
        if (req is ContextUser) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("userManger/addUser", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }
}

public client class UserMangerStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class UserMangerResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendResponse(response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextResponse(ContextResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextUser record {|
    User content;
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextResponse record {|
    response content;
    map<string|string[]> headers;
|};

public type User record {|
    string name = "";
    int user_id = 0;
    string password = "";
|};

public type response record {|
    string msg = "";
    User currentUser = {};
|};

const string ROOT_DESCRIPTOR = "0A0F757365725F696E666F2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F224F0A045573657212120A046E616D6518012001280952046E616D6512170A07757365725F69641802200128055206757365724964121A0A0870617373776F7264180320012809520870617373776F726422450A08726573706F6E736512100A036D736718012001280952036D736712270A0B63757272656E745573657218022001280B32052E55736572520B63757272656E7455736572325B0A0A757365724D616E676572121D0A0967726561747573657212052E557365721A092E726573706F6E7365122E0A076164645573657212052E557365721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "user_info.proto": "0A0F757365725F696E666F2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F224F0A045573657212120A046E616D6518012001280952046E616D6512170A07757365725F69641802200128055206757365724964121A0A0870617373776F7264180320012809520870617373776F726422450A08726573706F6E736512100A036D736718012001280952036D736712270A0B63757272656E745573657218022001280B32052E55736572520B63757272656E7455736572325B0A0A757365724D616E676572121D0A0967726561747573657212052E557365721A092E726573706F6E7365122E0A076164645573657212052E557365721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33"};
}

