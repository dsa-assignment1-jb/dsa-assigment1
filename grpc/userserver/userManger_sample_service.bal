import ballerina/grpc;
import ballerina/log;

listener grpc:Listener ep = new (9090);
User [] userList = [];
@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "userManger" on ep {
    

    remote function greatuser(User value) returns response|error {
        log:printInfo(value.name);
        response res = {msg:"Hello"+value.name,currentUser:value};
        return res;
    }
    remote function addUser(User value) returns string|error {
        userList.push(value);
        return "Sucessfully added user :"+(value.user_id).toString();

    }
}

