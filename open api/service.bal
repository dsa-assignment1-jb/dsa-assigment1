import ballerina/http;

import ballerina/io;

type Learner record {|
    string username;
    string lastname;
    string firstname;
    any preferred_formats = ["audio", "video", "text"];
    any past_subjects = [{
    course: "", 
    score: ""

}];
|};

type course record {|
    string course = "";
    any learning_objects = {
required: {
audio: [{
name: "", 
description: "", 
difficulty: " "
}], 
text: [{}]
}, 
suggested: {
video: [""], 
audio: [""]

        }
};
|};

public function main() returns error? {
    table<Learner> learner = table [
    {username: "har", firstname: "Harold", lastname: "styles", preferred_formats: (["adore you", "times", "grammy"]), past_subjects: ([{course: "Algo", score: "A+"}])}

]
;
}

public function lcourse() returns error? {
    table<course> course = table [
        {course: "Distributed Systems Applications", learning_objects: {required: ({audio: (["Topic 1", "Distributed System", "Hard"]), text: ({}), suggested: ({video: ([""]), audio: ([""])})})}}
    ];

}

Learner[] learners = [];
course[] courses = [];

service /vLearn on new http:Listener(9090) {

    resource function post addlearner(@http:Payload Learner new_learner) returns json {
        io:println("Processing new learner in learners");
        any res = learners.push(new_learner);
        return {"Add": "Successfully added" + new_learner.username + "to vitural learning"};

    }

    resource function get getCourse() returns course[] {
        io:println("Processing course work");
        return courses;
    }
    resource function put updatelearner(@http:Payload Learner updated_learner) returns Learner[] {
        int count = 1;
        while (count < learners.length()) {
            if (learners[count].username == updated_learner.username) {
                learners[count].firstname = updated_learner.firstname;
                learners[count].lastname = updated_learner.lastname;
                break;
            }
            count += 1;
        }
        return learners;

    }

}
